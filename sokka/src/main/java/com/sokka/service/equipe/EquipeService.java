package com.sokka.service.equipe;

import java.util.Date;
import java.util.List;
import com.sokka.model.Equipe;



public interface EquipeService {
	
	Equipe findEquipeById(Long id);
	List<Equipe> findAllEquipe();
	Equipe addEquipe(Equipe Equipe);
	List<Equipe> addEquipe(List<Equipe> Equipes);
	void deleteEquipe(Long idEquipe);
	void modifierNomEquipe(Long id,String nom, Boolean bool);
	void insert_eq_cat(Long idcat, Long idEq, Long score_elo, Long score_fair);
	Long getLastId_Equipe();
	
	boolean existsEquipeById(Long id);
	
	List<Equipe> getclassement(long id) ;
	
	List<Equipe> getlistAllAdversaire();
	
	List<Equipe> getEquipebydate(Date date1, Date date2);
	
}
