package com.sokka.service.equipe;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sokka.dao.EquipeDao;
import com.sokka.dao.historique.EquipeHistoriqueDao;
import com.sokka.model.Equipe;


@Service
public class EquipeServiceImpl implements  EquipeService{

	
	
	private final EquipeDao equipeDao;
	
	
	public EquipeServiceImpl(EquipeDao equipeDao) {
		super();
		this.equipeDao = equipeDao;
	}
	
	@Override
	public Equipe findEquipeById(Long id) {
		return equipeDao.findById(id).get();
	}
	/*
	@Override
	public Equipe findBestEquipe(Long id) {
		return equipeDao.findById(id).get();
	}*/

	@Override
	public List<Equipe> findAllEquipe() {
		return equipeDao.findAll();
	}

	@Override
	public Equipe addEquipe(Equipe Equipe) {
		return equipeDao.save(Equipe);
	}

	@Override
	public List<Equipe> addEquipe(List<Equipe> Equipes) {
		return equipeDao.saveAll(Equipes);
	}

	@Override
	public boolean existsEquipeById(Long id) {
		return equipeDao.existsById(id);
	}
	
	@Override
	public void deleteEquipe(Long idEquipe) {
		 equipeDao.delete(idEquipe);
		 equipeDao.delete_j(idEquipe);
		 equipeDao.deleteById(idEquipe);
	}
	
	
	@Override
	public List<Equipe> getclassement(long id){
		
		return equipeDao.getClassementAdversaires(id); // equipe classé par ordre 	
	}
	
	@Override 
	public List<Equipe> getlistAllAdversaire(){
		return equipeDao.getlistAllAdversaire();
	}

	@Override
	public List<Equipe> getEquipebydate(Date date1, Date date2){
		return equipeDao.getEquipebydate(date1,date2);
	}
	
	
	@Override
    public void modifierNomEquipe(Long id, String nom, Boolean bool) {
         equipeDao.Modifier_nom_equipe(id,nom,bool);
    }
	
	@Override
    public void insert_eq_cat(Long idcat, Long idEq, Long score_elo, Long score_fair) {
         equipeDao.Insert_equipe_categ(idcat, idEq, score_elo, score_fair);
    }
	
	
	@Override
    public Long getLastId_Equipe()	{
         return equipeDao.getLastId();
    }

}
