package com.sokka.service.joueur.historique;

import java.util.Date;
import java.util.List;

import com.sokka.model.historique.JoueurHistorique;

public interface JoueurHistoriqueService {
	JoueurHistorique addJoueurHistorique(JoueurHistorique joueurHistorique);
	public List<JoueurHistorique> getAllBestJoeurs(Date dateDebut, Date dateFin,String zone,int limit);
	public List<JoueurHistorique> findAllBestJoueurPeriode(Date dateDebut, Date dateFin,String zone,int limit);
	public List<JoueurHistorique> findBestJoueurinstant(Date dateFin,String zone);
	public List<JoueurHistorique> gethistoriquejoueur(long id, Date date1, Date date2);
	public JoueurHistorique ClassmentplusEleve(Long id);
}
