package com.sokka.service.joueur.historique;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sokka.model.Equipe;
import org.springframework.stereotype.Service;

import com.sokka.dao.historique.JoueurHistoriqueDao;
import com.sokka.model.historique.JoueurHistorique;


@Service
public class JoueurHistoriqueServiceImpl implements JoueurHistoriqueService{

	private final JoueurHistoriqueDao joueurHistoriqueDao;

	public JoueurHistoriqueServiceImpl(JoueurHistoriqueDao joueurHistoriqueDao) {
		super();
		this.joueurHistoriqueDao = joueurHistoriqueDao;
	}

	@Override
	public JoueurHistorique addJoueurHistorique(JoueurHistorique joueurHistorique) {
		return joueurHistoriqueDao.save(joueurHistorique);
	}
	
	@Override
	public List<JoueurHistorique> getAllBestJoeurs(Date dateDebut, Date dateFin,String zone,int limit) {
		List<JoueurHistorique> temp;
		if (dateDebut!=null && dateFin==null)
			temp = joueurHistoriqueDao.findDateDebutJoueur(dateDebut);
		else if (dateDebut==null && dateFin!=null)
			temp= joueurHistoriqueDao.findDateFinJoueur(dateFin);
		else if (dateDebut!=null && dateFin!=null)
			temp= joueurHistoriqueDao.findDateDebutEtFinJoueur(dateDebut,dateFin);
		else
			temp= joueurHistoriqueDao.findRecent();
		if (zone!=null){
			temp.removeIf(e->{
				boolean inZone=false;
				for (Equipe equipe :e.getJoueur().getEquipes())
				{
					if (!equipe.getZone().getVille().equals(zone)) {
						inZone = true;
						break;
					}
				}
				return inZone;
			});
		}
		List<JoueurHistorique> finalList= new ArrayList<JoueurHistorique>();
		if(temp.size()>limit) {
			for(int i = 0; i< limit; i++){
				finalList.add(temp.get(i));
			}
		}
		else finalList=temp;
		return finalList;
	}
	
	@Override
	public List<JoueurHistorique> findAllBestJoueurPeriode(Date dateDebut, Date dateFin,String zone,int limit) {
		List<JoueurHistorique> temp;
		temp= joueurHistoriqueDao.findDateDebutEtFinJoueur(dateDebut,dateFin);
		List<JoueurHistorique> finalList= new ArrayList<JoueurHistorique>();
		if(temp.size()>limit) {
			for(int i = 0; i< limit; i++){
				finalList.add(temp.get(i));
			}
		}
		else finalList=temp;
		return finalList;
	}
    
	@Override
	public List<JoueurHistorique> findBestJoueurinstant(Date dateFin,String zone){
		List<JoueurHistorique> temp;
		temp= joueurHistoriqueDao.findDateFinJoueur(dateFin);
		List<JoueurHistorique> finalList= new ArrayList<JoueurHistorique>();
		if(temp.size()>1) 
			finalList.add(temp.get(0));
		else
			finalList=temp;
		return finalList;
	}
	
	@Override
	public List<JoueurHistorique> gethistoriquejoueur(long id, Date date1, Date date2){
		return joueurHistoriqueDao.gethistoriquejoueur(id, date1, date2);
	}

	@Override
	public JoueurHistorique ClassmentplusEleve(Long id) {
		return joueurHistoriqueDao.plusEleve(id);
	}
}
