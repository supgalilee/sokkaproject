package com.sokka.service.historique;

import com.sokka.dao.ZoneDao;
import com.sokka.dao.historique.EquipeHistoriqueDao;
import com.sokka.dao.historique.EquipeHistoriqueDao.BestSerieDefaite;
import com.sokka.dao.historique.EquipeHistoriqueDao.BestSerieVictoire;
import com.sokka.dao.historique.EquipeHistoriqueDao.Progres;
import com.sokka.model.Zone;
import com.sokka.model.historique.EquipeHistorique;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class HistoriqueServiceImpl implements HistoriqueService {


	private final EquipeHistoriqueDao equipeHistoriqueDao;
	private final ZoneDao zoneDao;

	
	public HistoriqueServiceImpl(EquipeHistoriqueDao equipeHistoriqueDao,ZoneDao zoneDao) {
		super();
		this.equipeHistoriqueDao = equipeHistoriqueDao;
		this.zoneDao = zoneDao;
	}
   
	@Override
	public List<EquipeHistorique> findAllBestEquipe(Date dateDebut, Date dateFin, String nomZone,int limit) {
		List<EquipeHistorique> temp;
		if (dateDebut!=null && dateFin==null)
			temp = equipeHistoriqueDao.findDateDebut(dateDebut);
		else if (dateDebut==null && dateFin!=null)
			temp= equipeHistoriqueDao.findDateFin(dateFin);
		else if (dateDebut!=null && dateFin!=null)
			temp= equipeHistoriqueDao.findDateDebutEtFin(dateDebut,dateFin);
		else
		temp= equipeHistoriqueDao.findRecent();
		if (nomZone!=null)
			temp.removeIf(e -> !e.getEquipe().getZone().getVille().equals(nomZone));
		List<EquipeHistorique> finalList= new ArrayList<EquipeHistorique>();
		if(temp.size()>limit) {
			for(int i = 0; i< limit; i++){
				finalList.add(temp.get(i));
			}
		}
		else finalList=temp;
		return finalList;
	}
	
	@Override
	public List<EquipeHistorique> findAllBestEquipePeriode(Date dateDebut, Date dateFin, String nomZone, int limit) {
		List<EquipeHistorique> temp;
		temp= equipeHistoriqueDao.findDateDebutEtFin(dateDebut,dateFin);
		temp.removeIf(e -> !e.getEquipe().getZone().getVille().equals(nomZone));
		List<EquipeHistorique> finalList= new ArrayList<EquipeHistorique>();
		if(temp.size()>limit) {
			for(int i = 0; i< limit; i++){
				finalList.add(temp.get(i));
			}
		}
		else 
			finalList=temp;
		return finalList;
	}
    
	@Override
	public List<EquipeHistorique> findBestEquipeinstant(Date dateFin, String nomZone) {
		List<EquipeHistorique> temp;
		temp= equipeHistoriqueDao.findDateFin(dateFin);
		temp.removeIf(e -> !e.getEquipe().getZone().getVille().equals(nomZone));
		List<EquipeHistorique> finalList= new ArrayList<EquipeHistorique>();
		if(temp.size()>1) 
			finalList.add(temp.get(0));
		else
			finalList=temp;
		return finalList;
	}
    
	@Override
	public List<Zone> findAllZones() {
		return zoneDao.findAll();
	}

	@Override

	public List<EquipeHistorique> getEquipeHistorique(long id, Date date1, Date date2){
		return equipeHistoriqueDao.getEquipeHistorique(id, date1, date2);
	}

	public BestSerieVictoire findBestSerieVictoire(Long idE) {
		return equipeHistoriqueDao.BestSerieV_Dao(idE);
	}

	@Override
	public BestSerieDefaite findBestSerieDefaite(Long idE) {
		return equipeHistoriqueDao.BestSerieD_Dao(idE);
	}
	
	@Override
	public Long NbMatchEquipe(Long idE) {
		return equipeHistoriqueDao.NbMatch(idE);
	}

	@Override
	public Long pourcentageNbVictoire(Long idE) {
		Long pourcentage=(equipeHistoriqueDao.NbVictoire(idE)/equipeHistoriqueDao.NbMatch(idE));
		return (pourcentage*100);
	}

	@Override
	public Long pourcentageNbDefaite(Long idE) {
		Long pourcentage=(equipeHistoriqueDao.NbDefaite(idE)/equipeHistoriqueDao.NbMatch(idE));
		return (pourcentage*100);
	}

	@Override
	public List<Progres> AfficherProgresEquipe(Date dateDebut, Date dateFin,Long idE) {
		return equipeHistoriqueDao.AfficherProgres(dateDebut, dateFin, idE);
	}

	@Override
	public EquipeHistorique ClassmentPlusEleve(Long id) {
		
		return equipeHistoriqueDao.plusEleve(id);
	}
	
	
}
