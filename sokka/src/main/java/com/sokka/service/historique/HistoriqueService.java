package com.sokka.service.historique;

import com.sokka.dao.historique.EquipeHistoriqueDao.BestSerieDefaite;
import com.sokka.dao.historique.EquipeHistoriqueDao.BestSerieVictoire;
import com.sokka.dao.historique.EquipeHistoriqueDao.Progres;
import com.sokka.model.Zone;
import com.sokka.model.historique.EquipeHistorique;


import java.util.Date;
import java.util.List;


public interface HistoriqueService {
    List<EquipeHistorique> findAllBestEquipe(Date dateDebut,Date dateFin,String nomZone,int limit);
    List<EquipeHistorique> findAllBestEquipePeriode(Date dateDebut,Date dateFin,String nomZone,int limit);
    List<EquipeHistorique> findBestEquipeinstant(Date dateFin,String nomZone);
    BestSerieVictoire findBestSerieVictoire(Long idE);
    BestSerieDefaite findBestSerieDefaite(Long idE);
    Long NbMatchEquipe(Long idE);
    Long pourcentageNbVictoire(Long idE);
    Long pourcentageNbDefaite(Long idE);
    List<Progres> AfficherProgresEquipe(Date dateDebut,Date dateFin,Long idE);
    List<Zone> findAllZones();
    
    List<EquipeHistorique> getEquipeHistorique(long id, Date d1, Date d2); 
    EquipeHistorique ClassmentPlusEleve(Long id);
}
