package com.sokka.service.match;

import java.util.List;

import com.sokka.model.Match;

public interface MatchService {

	public Match addMatch(Match match);	
	List<Match> findAllMatch();
	public void Incrementer_compteur_victoire(Long id);
	public void Incrementer_compteur_defaite(Long id);
	public void Incrementer_serie_victoire(Long id);
	public void Incrementer_serie_defaite(Long id);
	public Long get_id_dom(Long id);
	public Long get_id_ext(Long id);
	public Long get_compteurVictoire(Long id);
	public Long get_compteurDefaite(Long id);
	public Long get_serieVictoire(Long id);
	public Long get_serieDefaite(Long id);



}