package com.sokka.service.match;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sokka.dao.MatchDao;
import com.sokka.model.Match;

@Service
public class MatchServiceImpl implements  MatchService{

	private final MatchDao matchDao;
	
	public MatchServiceImpl(MatchDao matchDao) {
		super();
		this.matchDao = matchDao;
	}
	
	@Override
	public List<Match> findAllMatch() {
		return matchDao.findAll();
	}
	
	@Override
	public Long get_id_dom(Long id) {
		return matchDao.Select_id_dom(id);
	}
	
	@Override
	public Long get_id_ext(Long id) {
		return matchDao.Select_id_ext(id);
	}
	
	@Override
	public Long get_compteurVictoire(Long id) {
		return matchDao.Select_compteur_victoire(id);
	}
	
	@Override
	public Long get_compteurDefaite(Long id) {
		return matchDao.Select_compteur_defaite(id);
	}
	@Override
	public Long get_serieVictoire(Long id) {
		return matchDao.Select_serie_victoire(id);
	}
	
	@Override
	public Long get_serieDefaite(Long id) {
		return matchDao.Select_serie_defaite(id);
	}
	
	
	@Override
	public Match addMatch(Match match) {
		return matchDao.save(match);
	}
	
	@Override
	public void Incrementer_compteur_victoire(Long id) {
		matchDao.Incrementer_compteur_victoire(id);
	}
	
	
	@Override
	public void Incrementer_compteur_defaite(Long id) {
		matchDao.Incrementer_compteur_defaite(id);
	}
	
	@Override
	public void Incrementer_serie_victoire(Long id) {
		matchDao.Incrementer_serieVictoire(id);
	}
	
	@Override
	public void Incrementer_serie_defaite(Long id) {
		matchDao.Incrementer_serieDefaite(id);
	}
	

	

}