package com.sokka.controller;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sokka.model.Equipe;
import com.sokka.model.historique.EquipeHistorique;
import com.sokka.service.equipe.EquipeService;
import com.sokka.service.historique.HistoriqueService;

@RestController
@RequestMapping(path = EquipeController.BASE_URL )
public class EquipeController {
	
	public static final String BASE_URL = "/sokka/api/v1/services/equipe";
	public static final String PathGetAllEquipes = "/all";
	public static final String PathGetEquipeByID = "/{id}";
	public static final String PathAddEquipe = "/add";
	public static final String PathAddListEquipe = "/add/list";
	public static final String PathGetClassementByID = "classement/{id}" ;
	public static final String PathGetEquipesProgression = "meilleuresprogression/{id}/{date1}/{date2}" ;
	public static final String DeleteEquipeById = "/delete/{id}";
	public static final String PathUpdateNomEquipe = "/update/{id}/{nom}/{bool}";

	public static final String PathGetClassementPe = "/ClassementPe/{id}" ;

	public static final String PathInsertEquipeCategorie = "/insert/{idcat}/{idequipe}/{score_elo}/{score_fair}";
	public static final String PathGetLastId = "/getId";

	public static final String PathUpdateEquipe = "/update";
	
	
	@Autowired
	private EquipeService equipeService;
	
	@Autowired
	private HistoriqueService historiqueService;

	public EquipeController(EquipeService equipeService) {
		super();
		this.equipeService = equipeService;
	}
	
	@GetMapping(path=EquipeController.PathGetAllEquipes)
	@CrossOrigin(origins ="http://localhost:4200")
	public List<Equipe> getAllEquipe(){
		return equipeService.findAllEquipe();
	}
	

	@CrossOrigin(origins ="http://localhost:4200")
	@GetMapping(path=EquipeController.PathGetEquipeByID)
	public Equipe getEquipeById(@PathVariable Long id){
		return equipeService.findEquipeById(id);
	}
	
	
	@GetMapping(path=EquipeController.PathGetLastId)
	@CrossOrigin(origins ="http://localhost:4200")
	public Long getLastId(){
		return equipeService.getLastId_Equipe();
	}
	
	
	@DeleteMapping(path=EquipeController.DeleteEquipeById)
	@CrossOrigin(origins ="http://localhost:4200")
	public void deletejoueur(@PathVariable Long id){
		 equipeService.deleteEquipe(id);	 
	}
	
	@PostMapping(path=EquipeController.PathAddEquipe)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public Equipe addEquipe(@RequestBody Equipe Equipe) {
		return equipeService.addEquipe(Equipe);
		
	}
	
	@PostMapping(path=EquipeController.PathInsertEquipeCategorie)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public void addEquipeCategorie(@PathVariable Long idcat, @PathVariable Long idequipe,@PathVariable Long score_elo, @PathVariable Long score_fair) {
		 equipeService.insert_eq_cat(idcat, idequipe, score_elo, score_fair);	
	}
	
	@PutMapping(path=EquipeController.PathUpdateNomEquipe)
	@CrossOrigin(origins ="http://localhost:4200")
    @ResponseStatus(HttpStatus.CREATED)
    public void updateNomEquipe(@PathVariable Long id, @PathVariable String nom,@PathVariable Boolean bool) {
        equipeService.modifierNomEquipe(id, nom, bool);

    }
	
	@PostMapping(path=EquipeController.PathAddListEquipe)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Equipe> addEquipe(@RequestBody List<Equipe> Equipes) {
		return equipeService.addEquipe(Equipes);
		
	}
	
	
	@PutMapping(path=EquipeController.PathUpdateEquipe)
	@ResponseStatus(HttpStatus.CREATED)
	public String updateEquipe(@RequestBody Equipe Equipe) {
		String msg =  "L'equipe d'id [" +Equipe.getIdEquipe() +"] n'existe pas !"; 
		if(equipeService.existsEquipeById(Equipe.getIdEquipe())) {
			equipeService.addEquipe(Equipe);
			msg = "Modification effectuée !";
		}
		return msg;
		
	}
	
	@GetMapping(path=EquipeController.PathGetClassementByID)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Equipe> getClassement(@PathVariable Long id){
		
		return equipeService.getclassement(id);
		
	}
	
	@GetMapping(path=EquipeController.PathGetEquipesProgression)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Equipe> nmeilleuresprogressions(@PathVariable("id") Long id, @PathVariable("date1") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date1, @PathVariable("date2") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date2) {
		int cumul, indice =0, m;
		List<Integer> liste = new ArrayList<Integer>();   
		List<Equipe> listeEquipe = equipeService.getEquipebydate(date1, date2);
		//System.out.println(" Date1 : "+ date1 + "date 2 : "+ date2 + " taille : "+ listeEquipe.size());
		List<Equipe> listeClassement = new ArrayList<Equipe>();
		
		if( id <= 0) {
			return listeClassement;
		}
		
		for(int i=0; i<listeEquipe.size();i++) {
			//recuperer l'historisation pour l'equipe i
			List<EquipeHistorique> eqh = historiqueService.getEquipeHistorique(listeEquipe.get(i).getIdEquipe(), date1, date2);
			cumul = 0;
			if (eqh.size() >= 2)
				for(int j=0; j<eqh.size()-1; j++) {
					//calculer l'évolution : Formule
					cumul =  (int) ( cumul - (eqh.get(j).getScoreEloEquipe() - eqh.get(j+1).getScoreEloEquipe() ) );
				}
			else
				cumul = 0 ;
			
			liste.add(cumul);			
		}

		while(liste.size() > 0 ) {		
			m = Collections.max(liste);
			for(int i=0; i<liste.size(); i++) {
				if( liste.get(i) >= m) {
					indice = i ;
					m = liste.get(i) ;
				}
			}	
			liste.remove( Collections.max(liste) ) ;
			listeClassement.add( listeEquipe.get(indice)) ; 
			listeEquipe.remove(listeEquipe.get(indice));
			if(listeClassement.size() == id) {
				break;
			}
		}
		
		return listeClassement;	
	}

	
	@GetMapping(path=EquipeController.PathGetClassementPe)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public Long getClassementPE(@PathVariable("id") Long id ) {
		return historiqueService.ClassmentPlusEleve(id).getScoreEloEquipe() ;
	}
	/*
	@GetMapping(path=EquipeController.PathGetClassementPe)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public EquipeHistorique getClassementPE(@PathVariable("id") Long id ) {
		return historiqueService.ClassmentPlusEleve(id) ;
	}
	*/
}
