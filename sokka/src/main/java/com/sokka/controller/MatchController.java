package com.sokka.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sokka.model.Match;
import com.sokka.service.match.MatchService;

//restcontroller
@RestController
@RequestMapping(path = MatchController.BASE_URL )
public class MatchController {
	
	public static final String BASE_URL = "/sokka/api/v1/services/match";
	public static final String PathAddMatch = "/add";
	public static final String PathGetAllMatchs = "/all";
	public static final String PathGetMatchById = "/all";

	@Autowired
	private MatchService matchService;

	public MatchController(MatchService match ) {
		super();
		this.matchService = match;
	}
	
	@GetMapping(path=MatchController.PathGetAllMatchs)
	@CrossOrigin(origins ="http://localhost:4200")
	public List<Match> getAllMatch(){
		return matchService.findAllMatch();
	}
	
	@PostMapping(path=MatchController.PathAddMatch)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public Match add_Match(@RequestBody Match match) {
		Match match2 = matchService.addMatch(match);
		Long matchId = match2.getIdMatch();
		System.out.println(matchId);
		Long id_dom = matchService.get_id_dom(matchId);
		//System.out.println(id_dom);

		Long id_ext = matchService.get_id_ext(matchId);
		//System.out.println(id_ext);

		Long compteur_vict_dom = matchService.get_compteurVictoire(id_dom);
		//System.out.println(compteur_vict_dom);

		Long compteur_defaite_dom = matchService.get_compteurDefaite(id_dom);
		//System.out.println(compteur_defaite_dom);

		Long compteur_vict_ext = matchService.get_compteurVictoire(id_ext);
		//System.out.println(compteur_vict_ext);

		Long compteur_defaite_ext = matchService.get_compteurDefaite(id_ext);
		//System.out.println(compteur_defaite_ext);

		Long serie_vict_dom = matchService.get_serieVictoire(id_dom);
		//System.out.println(serie_vict_dom);

		Long serie_defaite_dom = matchService.get_serieDefaite(id_dom);
		//System.out.println(serie_defaite_dom);

		Long serie_vict_ext = matchService.get_serieVictoire(id_ext);
		//System.out.println(serie_vict_ext);

		Long serie_defaite_ext = matchService.get_serieDefaite(id_ext);
		//System.out.println(serie_defaite_ext);




		if(match2.getNombreButEquipeA() > match2.getNombreButEquipeB()) {
			matchService.Incrementer_compteur_victoire(id_dom);
			matchService.Incrementer_compteur_defaite(id_ext);
			if(compteur_vict_dom+1 > serie_vict_dom) {
				matchService.Incrementer_serie_victoire(id_dom);
			}
			
			if(compteur_defaite_ext+1 > serie_defaite_ext) {
				matchService.Incrementer_serie_defaite(id_ext);
			}

		}
		else {
			matchService.Incrementer_compteur_victoire(id_ext);
			matchService.Incrementer_compteur_defaite(id_dom);
			if(compteur_vict_ext +1 > serie_vict_ext) {
				matchService.Incrementer_serie_victoire(id_ext);
			}
			if( compteur_defaite_dom+1  > serie_defaite_dom ) {
				matchService.Incrementer_serie_defaite(id_dom);
			}
			
		}
		return match2;
	}

}