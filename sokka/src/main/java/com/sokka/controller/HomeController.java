package com.sokka.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {
	
	@RequestMapping("/home")
	@ResponseBody
	public String home() {
		return "home";
	}
	
	@RequestMapping("/")
	@ResponseBody
	public String Empty() {
		return "hey";
	}
}
