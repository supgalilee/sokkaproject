package com.sokka.controller;

import com.sokka.model.historique.EquipeHistorique;
import com.sokka.service.historique.HistoriqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(path = HistoriqueController.BASE_URL )
public class HistoriqueController {

	public static final String BASE_URL = "/sokka/api/v1/services/historique";
	public static final String PathGetAllEquipes = "/equipes/all";
	public static final String PathGetAllBestEquipePeriode = "/equipes/NBest";
	public static final String PathGetBestEquipeInstant = "/equipes/Best";
	public static final String PathGetAllZones = "/zones/all";
	public static final String PathGetBestSerieVictoire = "/equipes/BestSerieVictoire";
	public static final String PathGetBestSerieDefaite= "/equipes/BestSerieDefaite";
	public static final String PathGetNbMatchJoueur= "/equipes/NBMatchs";
	public static final String PathGetPourcentageVictoire= "/equipes/PourcentageV";
	public static final String PathGetPourcentageDefaite= "/equipes/PourcentageD";
	public static final String PathGetProgres= "/equipes/Progres";
	
	
	@Autowired
	private HistoriqueService historiqueService;

	public HistoriqueController(HistoriqueService historiqueService) {
		super();
		this.historiqueService = historiqueService;
	}
	
	@GetMapping(path= HistoriqueController.PathGetAllEquipes)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?> getAllEquipe(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateDebut, @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateFin, @RequestParam(required = false) String nomZone,@RequestParam(required = false) int limit){
		return ResponseEntity.ok(historiqueService.findAllBestEquipe(dateDebut,dateFin,nomZone,limit));
	}

	
	@GetMapping(path= HistoriqueController.PathGetAllBestEquipePeriode)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?> getAllBestEquipePeriod(@RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateDebut, @RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateFin, @RequestParam(required = true) String nomZone,@RequestParam(required = true) int limit){
		return ResponseEntity.ok(historiqueService.findAllBestEquipePeriode(dateDebut,dateFin,nomZone,limit));
	}
	
	@GetMapping(path= HistoriqueController.PathGetBestEquipeInstant)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?> getAllBestEquipeInstan(@RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateFin, @RequestParam(required = true) String nomZone){
		return ResponseEntity.ok(historiqueService.findBestEquipeinstant(dateFin,nomZone));
	}
	
	@GetMapping(path= HistoriqueController.PathGetAllZones)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?> getAllZones(){
		return ResponseEntity.ok(historiqueService.findAllZones());
	}
	
	@GetMapping(path= HistoriqueController.PathGetBestSerieVictoire)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?>getBestSerieVictoire(@RequestParam(required = true) Long idE){
		return ResponseEntity.ok(historiqueService.findBestSerieVictoire(idE));
	}
	
	@GetMapping(path= HistoriqueController.PathGetBestSerieDefaite)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?>getBestSerieDefaite(@RequestParam(required = true) Long idE){
		return ResponseEntity.ok(historiqueService.findBestSerieDefaite(idE));
	}

	@GetMapping(path= HistoriqueController.PathGetNbMatchJoueur)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?>getNbMatchs(@RequestParam(required = true) Long idE){
		return ResponseEntity.ok(historiqueService.NbMatchEquipe(idE));
	}
	
	@GetMapping(path= HistoriqueController.PathGetPourcentageVictoire)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?>getPourcentageVictoire(@RequestParam(required = true) Long idE){
		return ResponseEntity.ok(historiqueService.pourcentageNbVictoire(idE));
	}
	
	@GetMapping(path= HistoriqueController.PathGetPourcentageDefaite)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?>getPourcentageDefaite(@RequestParam(required = true) Long idE){
		return ResponseEntity.ok(historiqueService.pourcentageNbDefaite(idE));
	}
	
	@GetMapping(path= HistoriqueController.PathGetProgres)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?> getAllProgre(@RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateDebut, @RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateFin,@RequestParam(required = true) Long idE){
		return ResponseEntity.ok(historiqueService.AfficherProgresEquipe(dateDebut, dateFin, idE));
	}
}
