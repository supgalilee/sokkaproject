package com.sokka.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sokka.model.Joueur;
import com.sokka.model.historique.JoueurHistorique;
import com.sokka.service.joueur.JoueurService;
import com.sokka.service.joueur.historique.JoueurHistoriqueService;

@RestController
@RequestMapping(path = JoueurController.BASE_URL )
public class JoueurController {
	
	public static final String BASE_URL = "/sokka/api/v1/services/joueur";
	public static final String PathGetAllJoueurs = "/all";
	public static final String PathGetJoueurByID = "/{id}";
	public static final String PathAddJoueur = "/add";
	public static final String PathAddListJoueur = "/add/list";
	
	
	public static final String PathAddJoueurToEquipe = "/equipe/add";//"/joueur/add/{idJoueur},{idEquipe}";
	public static final String PathGetAllJoueurEquipes = "/equipe/{id}";
	public static final String DeleteJoueurById = "/delete/{id}";
	public static final String PathGetJoueurProgression = "meilleuresprogression/{nombre}/{date1}/{date2}" ;
	public static final String PathUpdateJoueur = "/update";
	
	@Autowired
	private JoueurService joueurService;
	@Autowired
	private JoueurHistoriqueService joueurHistoriqueService;

	public JoueurController(JoueurService joueurService) {
		super();
		this.joueurService = joueurService;
	}
	
	@GetMapping(path=JoueurController.PathGetAllJoueurs)
	@CrossOrigin(origins ="http://localhost:4200")
	public List<Joueur> getAllJoueurs(){
		return joueurService.findAllJoueur();
	}
	
	@DeleteMapping(path=JoueurController.DeleteJoueurById)
	@CrossOrigin(origins ="http://localhost:4200")
	public void deletejoueur(@PathVariable Long id){
		 joueurService.deleteJoueur(id);
	}
	
	@CrossOrigin(origins ="http://localhost:4200")
	@GetMapping(path=JoueurController.PathGetJoueurByID)
	public Joueur getJoueurById(@PathVariable Long id){
		return joueurService.findJoueurById(id);
	}
	@GetMapping(path=JoueurController.PathGetAllJoueurEquipes)
	public List<Joueur> getAllJoueurEquipe(@PathVariable Long id){
		return joueurService.findJoueurEquipeById(id);
	}
	
	@PostMapping(path=JoueurController.PathAddJoueur)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public Joueur addJoueur(@RequestBody Joueur joueur) {
		return joueurService.addJoueur(joueur);
	}
	
	@PostMapping(path=JoueurController.PathAddListJoueur)
	@ResponseStatus(HttpStatus.CREATED)
	@CrossOrigin(origins ="http://localhost:4200")
	public List<Joueur> addListJoueur(@RequestBody List<Joueur> joueurs) {
		return joueurService.addJoueur(joueurs);
	}
	
	@PostMapping(path=JoueurController.PathAddJoueurToEquipe)
	@ResponseStatus(HttpStatus.CREATED)
	public String addJoueurToEquipe(@RequestParam(name = "idJoueur")String idJoueurString,@RequestParam("idEquipe")String idEquipeString) {
		Long idJoueur = Long.parseLong(idJoueurString) ;
		Long idEquipe = Long.parseLong(idEquipeString) ;
		String msg; 
		Integer res = joueurService.addJoueurToEquipe(idJoueur,idEquipe);
		if(res == 1 )msg = "le Joueur d'id : " + idJoueurString + " a été ajouté à l'équipe d'id : " + idEquipeString ;
		else msg = "Ajout Impossible !";
		return msg;
	}
	
	@PutMapping(path=JoueurController.PathUpdateJoueur)
	@ResponseStatus(HttpStatus.CREATED)
	public String updateJoueur(@RequestBody Joueur joueur) {
		String msg = "Le joueur d'id [" +joueur.getIdJoueur() +"] n'existe pas !"; 
		if(joueurService.existsJoueurById(joueur.getIdJoueur())) {
			// on fait une sauvegarde avant de modifier
			JoueurHistorique joueurHistorique = new JoueurHistorique(joueur);
			joueurHistoriqueService.addJoueurHistorique(joueurHistorique);
			
			joueurService.addJoueur(joueur);
			msg = "Modification effectuée !";
		}
		return msg;
	}
	
	
	@GetMapping(path=JoueurController.PathGetJoueurProgression)
	@ResponseStatus(HttpStatus.CREATED)
	@CrossOrigin(origins ="http://localhost:4200")
	public List<Joueur> nmeilleuresprogressions(@PathVariable("nombre") Long nombre, @PathVariable("date1") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date1, @PathVariable("date2") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date2) {
		
		int cumul;  int indice = 0 , m ;
		List<Integer> liste = new ArrayList<Integer>();  
		List<Joueur> listeJoueur = joueurService.getJoueurbydate(date1, date2);
		List<Joueur> listeClassement = new ArrayList<Joueur>();
		
		if( nombre <= 0) {
			return listeClassement;
		}
		
		for(int i=0; i<listeJoueur.size();i++) {
			//recuperer l'historisation pour le joueur i
			List<JoueurHistorique> jh = joueurHistoriqueService.gethistoriquejoueur(listeJoueur.get(i).getIdJoueur(), date1, date2);
			
			// System.out.println(" historique du joueur : "+ listeJoueur.get(i).getIdJoueur() +" \n " + jh);			
			cumul = 0;
			if(jh.size() >= 2)
				for(int j=0; j<jh.size()-1; j++) {
					//calculer l'évolution : Formule
					cumul =  (int) ( cumul - (jh.get(j).getScoreEloJoueur() - jh.get(j+1).getScoreEloJoueur() ) );
				}
			else
				cumul = 0 ;
			liste.add(cumul);
		}
		
		while(liste.size() > 0 ) {
			
			//System.out.println( "taille : " +  (cumulCr.size()));
			m = Collections.max(liste);
			for(int i=0; i<liste.size(); i++) {
				
				if( liste.get(i) >= m) {
					//System.out.println(liste.get(i) + " : "+ m);
					indice = i ;
					m = liste.get(i) ;
				}
			}	
			liste.remove( Collections.max(liste) ) ;
			listeClassement.add( listeJoueur.get(indice)) ; 
			listeJoueur.remove(listeJoueur.get(indice));
			
			if(listeClassement.size() == nombre) {
				break;
			}
		}
		
		return listeClassement;	
	}
	
}
