package com.sokka.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sokka.model.historique.EquipeHistorique;
import com.sokka.model.historique.JoueurHistorique;
import com.sokka.service.historique.HistoriqueService;
import com.sokka.service.joueur.historique.JoueurHistoriqueService;

@RestController
@RequestMapping(path = JoueurHistoriqueController.BASE_URL )
public class JoueurHistoriqueController {
	
	public static final String BASE_URL = "/sokka/api/v1/services/historiqueJoueur";
	public static final String PathGetAllJoueurs = "/joueurs/all";
	public static final String PathGetAllBestJoueursPeriode = "/joueurs/NBest";
	public static final String PathGetBestJoueurInstant = "/joueurs/Best";
	public static final String PathClassementPe = "/ClassementPe/{id}";
	
	@Autowired
	private JoueurHistoriqueService joueurHistoriqueService;

	public JoueurHistoriqueController(JoueurHistoriqueService joueurHistoriqueService) {
		super();
		this.joueurHistoriqueService = joueurHistoriqueService;
	}
	
	@GetMapping(path= JoueurHistoriqueController.PathGetAllJoueurs)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?> getAllBestJoeurs(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateDebut, @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateFin, @RequestParam(required = false) String zone,@RequestParam(required = true) int limit){
		return ResponseEntity.ok(joueurHistoriqueService.getAllBestJoeurs(dateDebut,dateFin,zone,limit));
	}
	
	@GetMapping(path= JoueurHistoriqueController.PathGetAllBestJoueursPeriode)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?> findAllBestJoueurPeriod(@RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateDebut, @RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateFin, @RequestParam(required = true) String zone,@RequestParam(required = true) int limit){
		return ResponseEntity.ok(joueurHistoriqueService.findAllBestJoueurPeriode(dateDebut,dateFin,zone,limit));
	}
	
	@GetMapping(path= JoueurHistoriqueController.PathGetBestJoueurInstant)
	@CrossOrigin(origins ="http://localhost:4200")
	public ResponseEntity<?> findBestJoueurinstan(@RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateFin, @RequestParam(required = true) String zone){
		return ResponseEntity.ok(joueurHistoriqueService.findBestJoueurinstant(dateFin,zone));
	}
	
	/*
	@GetMapping(path=EquipeController.PathGetClassementPe)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public JoueurHistorique getClassementPE(@PathVariable("id") Long id ) {
		return joueurHistoriqueService.ClassmentplusEleve(id);
	}
	*/
	
	@GetMapping(path=EquipeController.PathGetClassementPe)
	@CrossOrigin(origins ="http://localhost:4200")
	@ResponseStatus(HttpStatus.CREATED)
	public Long getClassementPE(@PathVariable("id") Long id ) {
		return joueurHistoriqueService.ClassmentplusEleve(id).getScoreEloJoueur();
	}
}
