package com.sokka.dao;



import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sokka.model.Match;


@Repository
public interface MatchDao extends JpaRepository<Match,Long>{
	

	@Transactional
	@Modifying
	@Query(value = "UPDATE equipe "
			+ "SET compteur_victoire = compteur_victoire+1 , compteur_defaite = 0  "
			+ "WHERE equipe.idEquipe = :id " 
			, nativeQuery = true)
	void Incrementer_compteur_victoire(@Param("id") Long id) ; 
	

	
	@Transactional
	@Modifying
	@Query(value = "UPDATE equipe "
			+ "SET compteur_defaite = compteur_defaite+1, compteur_victoire = 0  "
			+ "WHERE equipe.idEquipe = :id " 
			, nativeQuery = true)
	void Incrementer_compteur_defaite(@Param("id") Long id) ; 
	
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE equipe "
			+ "SET serie_victoire = compteur_victoire  "
			+ "WHERE equipe.idEquipe = :id " 
			, nativeQuery = true)
	void Incrementer_serieVictoire(@Param("id") Long id) ; 
	
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE equipe "
			+ "SET serie_defaite = compteur_defaite  "
			+ "WHERE equipe.idEquipe = :id " 
			, nativeQuery = true)
	void Incrementer_serieDefaite(@Param("id") Long id) ; 
	

	@Query(value = "SELECT idEquipeDomicile from matchs "
			+ "WHERE matchs.idMatch = :id " 
			, nativeQuery = true)
	public Long  Select_id_dom(@Param("id") Long id) ; 
	
	@Query(value = "SELECT idEquipeExterieur from matchs "
			+ "WHERE matchs.idMatch = :id " 
			, nativeQuery = true)
	public Long Select_id_ext(@Param("id") Long id) ; 
	
	
	@Query(value = "SELECT compteur_victoire from equipe "
			+ "WHERE equipe.idEquipe = :id " 
			, nativeQuery = true)
	public Long Select_compteur_victoire(@Param("id") Long id) ; 
	
	@Query(value = "SELECT compteur_defaite from equipe "
			+ "WHERE equipe.idEquipe = :id " 
			, nativeQuery = true)
	public Long Select_compteur_defaite(@Param("id") Long id) ; 
	
	@Query(value = "SELECT serie_victoire from equipe "
			+ "WHERE equipe.idEquipe = :id " 
			, nativeQuery = true)
	public Long Select_serie_victoire(@Param("id") Long id) ; 
	
	@Query(value = "SELECT serie_defaite from equipe "
			+ "WHERE equipe.idEquipe = :id " 
			, nativeQuery = true)
	public Long Select_serie_defaite(@Param("id") Long id) ; 
	

	
	
	
	
	
}