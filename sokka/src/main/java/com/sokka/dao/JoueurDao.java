package com.sokka.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sokka.model.Joueur;

@Repository
public interface JoueurDao extends JpaRepository<Joueur,Long>{
	
	//public Joueur findJoueurByIdString(String id);
	
	@Query(value = "SELECT * FROM joueur WHERE idJoueur IN ( SELECT idJoueur FROM equipe_joueur WHERE idEquipe = ?1 ) ", nativeQuery = true)
	List<Joueur> findJoueurEquipeById_Dao(Long id);
	
	@Modifying
	@Query(value = "INSERT INTO equipe_joueur (idJoueur, idEquipe) values ( :idJoueur , :idEquipe ) ", nativeQuery = true)
	@Transactional
	Integer addJoueurToEquipe_Dao(@Param("idJoueur") Long idJoueur, @Param("idEquipe") Long idEquipe);


	@Query(value = "SELECT DISTINCT * "
			+ "FROM joueur "
			+ " ", nativeQuery = true)
	List<Joueur> getlistAllJoueurs();

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM joueur_historique WHERE joueur_historique.idJoueurH= :id", nativeQuery = true)
	void delete(Long id);
	
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM equipe_joueur WHERE equipe_joueur.idJoueur= :id", nativeQuery = true)
	void delete_j(Long id);
	
	@Query(value = " SELECT DISTINCT j1.* "
			+ " FROM joueur_historique j, Joueur j1 "
			+ " WHERE j.idJoueur = j1.idJoueur AND j.dateModification >= :dateDebut AND j.dateModification <= :dateFin ", nativeQuery = true)
	List<Joueur> getJoueurbydate(@Param("dateDebut") Date date1, @Param("dateFin") Date date2); 

}

//public interface JoueurDao extends CrudRepository<Joueur,Long>{
//	
//
//}