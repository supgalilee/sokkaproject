package com.sokka.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sokka.model.Equipe;
import com.sokka.model.EquipeCategorie;

@Repository
public interface EquipeDao extends JpaRepository<Equipe,Long>{

	@Query(value = "SELECT DISTINCT e2.* "
			+ "FROM equipe e1, equipe e2, matchs "
			+ "WHERE ( e1.idEquipe = :id AND e1.idEquipe = matchs.idEquipeExterieur  AND e2.idEquipe = matchs.idEquipeDomicile )" 
			+ "       OR "
			+ "      ( e1.idEquipe = :id AND e1.idEquipe = matchs.idEquipeDomicile AND e2.idEquipe = matchs.idEquipeExterieur )"						
			, nativeQuery = true)
	List<Equipe> selectEquipeAdversaire(@Param("id") Long id) ; 
	
	@Query(value = " SELECT ec.ScoreEloEquipe "
			+      " FROM equipe e, equipe_categorie ec "
			+      " WHERE e.idEquipe = ec.idEquipe AND ec.idEquipe = :id "
			, nativeQuery = true)
	@Transactional
	long getScoreforEquipe (@Param("id") Long id);
	
	@Query(value = "SELECT DISTINCT e.* "
			+ " FROM equipe e, equipe_categorie ec"
			+ " WHERE e.idEquipe = ec.idEquipe AND ec.ScoreEloEquipe = :scoreE"
			, nativeQuery = true)
	List<Equipe> getEquipe(@Param("scoreE") Long scoreE) ;
	
	@Query(value = "SELECT DISTINCT e2.* "
			+ "FROM equipe_categorie ec, equipe e1, equipe e2, matchs m "
			+ "WHERE ( e1.idEquipe = :id AND e1.idEquipe = m.idEquipeExterieur  AND e2.idEquipe = m.idEquipeDomicile AND e2.idEquipe = ec.idEquipe ) "
			+ "  OR "
			+ "     ( e1.idEquipe = :id AND e1.idEquipe = m.idEquipeDomicile AND e2.idEquipe = m.idEquipeExterieur AND e2.idEquipe = ec.idEquipe ) "
			+ "ORDER BY ec.ScoreEloEquipe DESC", nativeQuery = true) 
	List<Equipe> getClassementAdversaires(@Param("id") long id) ;

	@Query(value = " SELECT DISTINCT e1.* "
			+ " FROM equipe_historique e, equipe e1 "
			+ " WHERE e.idEquipe = e1.idEquipe AND e.dateModificationScore >= :dateDebut AND e.dateModificationScore <= :dateFin ", nativeQuery = true)
	List<Equipe> getEquipebydate(@Param("dateDebut") Date date1, @Param("dateFin") Date date2); 


	/*
	@Query(value = "SELECT * FROM joueur WHERE idJoueur IN ( SELECT idJoueur FROM equipe_joueur WHERE idEquipe = ?1 ) ", nativeQuery = true)
	Equipe findBestEquipe(Long id);
	 */
	
	@Query(value = "SELECT DISTINCT * "
			+ "FROM equipe "
			+ " ", nativeQuery = true)
	List<Equipe> getlistAllAdversaire();

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM equipe_historique WHERE equipe_historique.idEquipe= :id", nativeQuery = true)
	void delete(Long id);
	 
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM equipe_joueur WHERE equipe_joueur.idEquipe= :id", nativeQuery = true)
	void delete_j(Long id);

	
	@Transactional
    @Modifying
    @Query(value = "UPDATE equipe " 
            + "SET nomEquipe= :nom , estDispoPourMatch= :bool "
            + "WHERE idEquipe= :id"
            , nativeQuery = true)
    void Modifier_nom_equipe(@Param("id") Long id, @Param("nom") String nom, @Param("bool") Boolean bool);
	
	@Transactional
    @Modifying
    @Query(value = "INSERT INTO equipe_categorie " 
            + " VALUES (:sc_elo, :sc_fair, :idEquipe, :idCategorie ) "
            , nativeQuery = true)
    void Insert_equipe_categ(@Param("idCategorie") Long id, @Param("idEquipe") Long idEquipe, @Param("sc_elo") Long score, @Param("sc_fair") Long score_fair);
	
	
	@Transactional
    @Query(value = "SELECT MAX(idEquipe) FROM equipe " 
            , nativeQuery = true)
    Long getLastId();
	
	
}
