package com.sokka.dao.historique;

import com.sokka.model.historique.EquipeHistorique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

@Repository
public interface EquipeHistoriqueDao extends JpaRepository<EquipeHistorique,Long>{

   @Query(value = "SELECT t1.* FROM equipe_historique t1 WHERE t1.dateModification = (SELECT MAX(t2.dateModification) FROM equipe_historique t2 WHERE t2.nomEquipe = t1.nomEquipe AND t2.dateModification>= ?1 AND t2.dateModification <= ?2 AND t2.attributModifie=1) ORDER BY ScoreEloEquipe DESC", nativeQuery = true)
   List<EquipeHistorique> findDateDebutEtFin(Date dateDebut,Date dateFin);

   @Query(value = "SELECT t1.* FROM equipe_historique t1 WHERE t1.dateModification = (SELECT MAX(t2.dateModification) FROM equipe_historique t2 WHERE t2.nomEquipe = t1.nomEquipe AND t2.dateModification<= ?1 AND t2.attributModifie=1) ORDER BY ScoreEloEquipe DESC", nativeQuery = true)
   List<EquipeHistorique> findDateFin(Date dateFin);
   
   
   @Query(value = "SELECT t1.* FROM equipe_historique t1 WHERE t1.dateModification = (SELECT MIN(t2.dateModification) FROM equipe_historique t2 WHERE t2.nomEquipe = t1.nomEquipe AND t2.dateModification>= ?1 AND t2.attributModifie=1) ORDER BY ScoreEloEquipe DESC", nativeQuery = true)
   List<EquipeHistorique> findDateDebut(Date dateDebut);

   @Query(value = "SELECT t1.* FROM equipe_historique t1 WHERE t1.dateModificationScore = (SELECT MAX(t2.dateModificationScore) FROM equipe_historique t2 WHERE t2.nomEquipe = t1.nomEquipe) ORDER BY ScoreEloEquipe DESC limit ?1", nativeQuery = true)
   List<EquipeHistorique> findRecent(int limit);
   
   @Query(value = "SELECT *"
			+ "FROM equipe_historique "
			+ "WHERE idEquipe = :id AND dateModificationScore >= :dateDebut AND dateModificationScore <= :dateFin"
			+ " ", nativeQuery = true)
	List<EquipeHistorique> getEquipeHistorique(@Param("id") long id, @Param("dateDebut") Date date1, @Param("dateFin") Date date2); 


   @Query(value = "SELECT t1.* FROM equipe_historique t1 WHERE t1.dateModification = (SELECT MAX(t2.dateModification) FROM equipe_historique t2 WHERE t2.nomEquipe = t1.nomEquipe AND t2.attributModifie=1) ORDER BY ScoreEloEquipe DESC", nativeQuery = true)
   List<EquipeHistorique> findRecent();
  
   @Query(value = " SELECT nomEquipe as nomEquipe, MAX(serie_victoire) as serie_victoire FROM equipe_historique where idEquipe = ?1", nativeQuery = true)
   @Transactional
   BestSerieVictoire BestSerieV_Dao(Long idE);
   public static interface BestSerieVictoire {
	     String getNomEquipe();
	     String getSerie_victoire();
	  }
   
   @Query(value = " SELECT nomEquipe as nomEquipe, Min(serie_defaite) as serie_defaite FROM equipe_historique where idEquipe = ?1", nativeQuery = true)
   @Transactional
   BestSerieDefaite BestSerieD_Dao(Long idE);
   public static interface BestSerieDefaite {
	     String getNomEquipe();
	     String getSerie_defaite();
	  }
   
   @Query(value = "select count(*) from matchs where idEquipeDomicile= ?1 or idEquipeExterieur=?1", nativeQuery = true)
   @Transactional
   Long NbMatch(Long idE);  
   
   @Query(value = "SELECT SUM(serie_victoire) AS nbVictoire FROM equipe_historique where idEquipe =?1", nativeQuery = true)
   @Transactional
   Long NbVictoire(Long idE);   
   
   @Query(value = "SELECT SUM(serie_defaite) AS nbDefaite FROM equipe_historique where idEquipe =?1", nativeQuery = true)
   @Transactional
   Long NbDefaite(Long idE); 
   
   @Query(value = " SELECT ScoreEloEquipe, dateModification FROM equipe_historique WHERE dateModification >= ?1 and dateModification <= ?2 and attributModifie=1 and idEquipe= ?3 order by dateModification", nativeQuery = true)
   @Transactional
   List<Progres> AfficherProgres(Date dateDebut,Date dateFin,Long idE);
   public static interface Progres {
	     String getScoreEloEquipe();
	     Date getdateModification();
	  }
   
   @Query(value=" SELECT DISTINCT e.* "
   		+ " FROM equipe_historique e"
   		+ " WHERE e.ScoreEloEquipe = ( SELECT MAX(e1.ScoreEloEquipe) " 
   		+ " FROM equipe_historique e1  WHERE  e1.idEquipe = ?1 ) AND e.idEquipe = ?1 ORDER BY ScoreEloEquipe DESC", nativeQuery = true)
   @Transactional
   EquipeHistorique plusEleve(Long id);   
}
