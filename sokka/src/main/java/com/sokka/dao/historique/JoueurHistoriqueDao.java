package com.sokka.dao.historique;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sokka.model.historique.EquipeHistorique;
import com.sokka.model.historique.JoueurHistorique;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

@Repository
public interface JoueurHistoriqueDao extends JpaRepository<JoueurHistorique,Long>{


    @Query(value = "SELECT t1.* FROM joueur_historique t1 WHERE t1.dateModification = (SELECT MAX(t2.dateModification) FROM joueur_historique t2 inner join equipe_joueur j on j.idJoueur=t2.idJoueurH inner join equipe e on j.idEquipe=e.idEquipe inner join zone z on e.idZone=z.idZone WHERE t2.nomJoueur = t1.nomJoueur AND t2.dateModification> ?1 AND t2.dateModification < ?2 AND z.ville=?3) ORDER BY scoreEloJoueur DESC limit ?4", nativeQuery = true)
    List<JoueurHistorique> findDateDebutEtFinJoueur(Date dateDebut, Date dateFin,String zone, int limit);

    @Query(value = "SELECT t1.* FROM joueur_historique t1 WHERE t1.dateModification =(SELECT MAX(t2.dateModification) FROM joueur_historique t2 inner join equipe_joueur j on j.idJoueur=t2.idJoueurH inner join equipe e on j.idEquipe=e.idEquipe inner join zone z on e.idZone=z.idZone WHERE t2.nomJoueur = t1.nomJoueur AND t2.dateModification<= ?1 AND z.ville=?2) ORDER BY scoreEloJoueur DESC limit ?3", nativeQuery = true)
    List<JoueurHistorique> findDateFinJoueur(Date dateFin,String zone,int limit);
    
    @Query(value = "SELECT t1.* FROM joueur_historique t1 WHERE t1.dateModification = (SELECT MAX(t2.dateModification) FROM joueur_historique t2 inner join equipe_joueur j on j.idJoueur=t2.idJoueurH inner join equipe e on j.idEquipe=e.idEquipe inner join zone z on e.idZone=z.idZone WHERE t2.nomJoueur = t1.nomJoueur AND z.ville=?1) ORDER BY scoreEloJoueur DESC limit ?2", nativeQuery = true)
    List<JoueurHistorique> findRecentJoueur(String zone,int limit);
    
    @Query(value = "SELECT t1.* FROM joueur_historique t1 WHERE t1.dateModification = ( SELECT MAX(t2.dateModification) FROM joueur_historique t2 WHERE t2.nomJoueur = t1.nomJoueur ) ORDER BY scoreEloJoueur DESC limit ?1", nativeQuery = true)
    List<JoueurHistorique> findRecent(int limit);
    
    @Query(value = "SELECT *"
			+ "FROM joueur_historique "
			+ "WHERE idjoueur = :id AND dateModification >= :dateDebut AND dateModification <= :dateFin"
			+ " ", nativeQuery = true)
	List<JoueurHistorique> gethistoriquejoueur(@Param("id") long id, @Param("dateDebut") Date date1, @Param("dateFin") Date date2); 


    @Query(value = "SELECT t1.* FROM joueur_historique t1 WHERE t1.dateModification = (SELECT MAX(t2.dateModification) FROM joueur_historique t2 WHERE t2.nomJoueur = t1.nomJoueur AND t2.dateModification>= ?1 AND t2.dateModification <= ?2) ORDER BY scoreEloJoueur DESC", nativeQuery = true)
    List<JoueurHistorique> findDateDebutEtFinJoueur(Date dateDebut, Date dateFin);

    @Query(value = "SELECT t1.* FROM joueur_historique t1 WHERE t1.dateModification = (SELECT MAX(t2.dateModification) FROM joueur_historique t2 WHERE t2.nomJoueur = t1.nomJoueur AND t2.dateModification<= ?1) ORDER BY scoreEloJoueur DESC", nativeQuery = true)
    List<JoueurHistorique> findDateFinJoueur(Date dateFin);


    @Query(value = "SELECT t1.* FROM joueur_historique t1 WHERE t1.dateModification = (SELECT MIN(t2.dateModification) FROM joueur_historique t2 WHERE t2.nomJoueur = t1.nomJoueur AND t2.dateModification>= ?1) ORDER BY scoreEloJoueur DESC", nativeQuery = true)
    List<JoueurHistorique> findDateDebutJoueur(Date dateDebut);

    @Query(value = "SELECT t1.* FROM joueur_historique t1 WHERE t1.dateModification = (SELECT MAX(t2.dateModification) FROM joueur_historique t2 WHERE t2.nomJoueur = t1.nomJoueur) ORDER BY scoreEloJoueur DESC", nativeQuery = true)
    List<JoueurHistorique> findRecent();
    
    @Query(value=" SELECT DISTINCT j.* "
    		+ "FROM joueur_historique j WHERE j.ScoreEloJoueur = ( SELECT MAX(j1.ScoreEloJoueur) "
    		+ "FROM joueur_historique j1   WHERE  j1.idJoueur = ?1 ) AND j.idJoueur = ?1 " 
    		+ "ORDER BY ScoreEloJoueur DESC", nativeQuery = true)
    @Transactional
    JoueurHistorique plusEleve(Long id);
}
