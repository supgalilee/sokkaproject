package com.sokka.dao;

import com.sokka.model.Zone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZoneDao extends JpaRepository<Zone,Long>{
	List<Zone> findAll();
}
