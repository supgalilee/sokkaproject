package com.sokka.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "zone")
public class Zone {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idZone;
	private String ville;
	private String departement;
	private double latitude;
	private double longitude;
	private double altitude;
	
	@OneToMany(mappedBy="zone")
	@JsonBackReference
    private Set<Equipe> equipes;
	
	public Zone() {
		super();
	}
	
	public Zone(Long idZone, String ville, String departement, double latitude, double longitude, double altitude,
			Set<Equipe> equipes) {
		super();
		this.idZone = idZone;
		this.ville = ville;
		this.departement = departement;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.equipes = equipes;
	}

	public Long getIdZone() {
		return idZone;
	}
	public void setIdZone(Long idZone) {
		this.idZone = idZone;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getDepartement() {
		return departement;
	}
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getAltitude() {
		return altitude;
	}
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public Set<Equipe> getEquipes() {
		return equipes;
	}

	public void setEquipes(Set<Equipe> equipes) {
		this.equipes = equipes;
	}

	@Override
	public String toString() {
		return "Zone [idZone=" + idZone + ", ville=" + ville + ", departement=" + departement + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", altitude=" + altitude + ", equipes=" + equipes + "]";
	}
    


}
