package com.sokka.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "terrain")
public class Terrain {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idTerrain;	
	private String nomTerrain;	
	private String numeroDeVoie;
	private String libelle;
	private String autreInfo;	
	private String typeTerrain;
	private Long dimensionLongueur; 
	private Long dimensionLargeur; 
	private Boolean estSupprime;
		
	@OneToMany(mappedBy = "terrain", cascade = CascadeType.ALL)
    private Set<Match> match;
	
	
	public Terrain() {
		super();
	}
	
	public Terrain(Long idTerrain, String nomTerrain, String numeroDeVoie, String libelle, String autreInfo,
			String typeTerrain, Long dimensionLongueur, Long dimensionLargeur, Boolean estSupprime, Set<Match> match) {
		super();
		this.idTerrain = idTerrain;
		this.nomTerrain = nomTerrain;
		this.numeroDeVoie = numeroDeVoie;
		this.libelle = libelle;
		this.autreInfo = autreInfo;
		this.typeTerrain = typeTerrain;
		this.dimensionLongueur = dimensionLongueur;
		this.dimensionLargeur = dimensionLargeur;
		this.estSupprime = estSupprime;
		this.match = match;
	}
	public Long getIdTerrain() {
		return idTerrain;
	}
	public void setIdTerrain(Long idTerrain) {
		this.idTerrain = idTerrain;
	}
	public String getNomTerrain() {
		return nomTerrain;
	}
	public void setNomTerrain(String nomTerrain) {
		this.nomTerrain = nomTerrain;
	}
	public String getNumeroDeVoie() {
		return numeroDeVoie;
	}
	public void setNumeroDeVoie(String numeroDeVoie) {
		this.numeroDeVoie = numeroDeVoie;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getAutreInfo() {
		return autreInfo;
	}
	public void setAutreInfo(String autreInfo) {
		this.autreInfo = autreInfo;
	}
	public String getTypeTerrain() {
		return typeTerrain;
	}
	public void setTypeTerrain(String typeTerrain) {
		this.typeTerrain = typeTerrain;
	}
	public Long getDimensionLongueur() {
		return dimensionLongueur;
	}
	public void setDimensionLongueur(Long dimensionLongueur) {
		this.dimensionLongueur = dimensionLongueur;
	}
	public Long getDimensionLargeur() {
		return dimensionLargeur;
	}
	public void setDimensionLargeur(Long dimensionLargeur) {
		this.dimensionLargeur = dimensionLargeur;
	}

	public Boolean getestSupprime() {
		return estSupprime;
	}
	public void setestSupprime(Boolean estSupprime) {
		this.estSupprime = estSupprime;
	}
	@Override
	public String toString() {
		return "Terrain [idTerrain=" + idTerrain + ", nomTerrain=" + nomTerrain + ", numeroDeVoie=" + numeroDeVoie
				+ ", libelle=" + libelle + ", autreInfo=" + autreInfo + ", typeTerrain=" + typeTerrain
				+ ", dimensionLongueur=" + dimensionLongueur + ", dimensionLargeur=" + dimensionLargeur
				+ ", estSupprime=" + estSupprime + ", match=" + match + "]";
	}

		

}
