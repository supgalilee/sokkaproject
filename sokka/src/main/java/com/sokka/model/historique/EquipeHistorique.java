package com.sokka.model.historique;

import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import com.sokka.model.Equipe;


@Entity
@Table(name = "equipe_historique")
public class EquipeHistorique {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idEquipeHistorique;
	private Date dateModification;
	private String idStringEquipe;
	private String nomEquipe;
	private Boolean estDispoPourMatch;
	private Boolean estSupprime;
	private Long scoreEloEquipe;
	private Long scroreFairPlayEquipe;
	private String imageEquipe;
	private Long serie_victoire;
	private Long serie_defaite;
	
	/* si la modification est au niveau de score attributModifie prend 1
	si la modification est au niveau de serie de defaite prend 2
	si la modification est au niveau de serie de victoire prend 3
	    une fois l'equipe perd, la serie de victoire est enregistrer dans la base de données 
	avec le score elo de la valeur precedente (plus recente) et de meme pour la sere de defaite	
	*/
	private Long attributModifie;
	
	@ManyToOne
    @JoinColumn(name="idEquipe", nullable=false)
	//@JsonManagedReference       
	private Equipe equipe;

	public EquipeHistorique() {
		super();
	}


	public EquipeHistorique(Long idEquipeHistorique, Date dateModification, String idStringEquipe, String nomEquipe,
			Boolean estDispoPourMatch, Boolean estSupprime, Long scoreEloEquipe, Long scroreFairPlayEquipe,
			String imageEquipe, Long serie_victoire, Long serie_defaite, Long attributModifie, Equipe equipe) {
		super();
		this.idEquipeHistorique = idEquipeHistorique;
		this.dateModification = dateModification;
		this.idStringEquipe = idStringEquipe;
		this.nomEquipe = nomEquipe;
		this.estDispoPourMatch = estDispoPourMatch;
		this.estSupprime = estSupprime;
		this.scoreEloEquipe = scoreEloEquipe;
		this.scroreFairPlayEquipe = scroreFairPlayEquipe;
		this.imageEquipe = imageEquipe;
		this.serie_victoire = serie_victoire;
		this.serie_defaite = serie_defaite;
		this.attributModifie = attributModifie;
		this.equipe = equipe;
	}



	public Long getSerie_victoire() {
		return serie_victoire;
	}

	public void setSerie_victoire(Long serie_victoire) {
		this.serie_victoire = serie_victoire;
	}

	public Long getSerie_defaite() {
		return serie_defaite;
	}

	public void setSerie_defaite(Long serie_defaite) {
		this.serie_defaite = serie_defaite;
	}

	public Long getAttributModifie() {
		return attributModifie;
	}
	
	public void setAttributModifie(Long attributModifie) {
		this.attributModifie = attributModifie;
	}

	public Long getIdEquipeHistorique() {
		return idEquipeHistorique;
	}

	public void setIdEquipeHistorique(Long idEquipeHistorique) {
		this.idEquipeHistorique = idEquipeHistorique;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public String getIdStringEquipe() {
		return idStringEquipe;
	}

	public void setIdStringEquipe(String idStringEquipe) {
		this.idStringEquipe = idStringEquipe;
	}

	public String getNomEquipe() {
		return nomEquipe;
	}

	public void setNomEquipe(String nomEquipe) {
		this.nomEquipe = nomEquipe;
	}

	public Boolean getEstDispoPourMatch() {
		return estDispoPourMatch;
	}

	public void setEstDispoPourMatch(Boolean estDispoPourMatch) {
		this.estDispoPourMatch = estDispoPourMatch;
	}

	public Boolean getEstSupprime() {
		return estSupprime;
	}

	public void setEstSupprime(Boolean estSupprime) {
		this.estSupprime = estSupprime;
	}

	public Long getScoreEloEquipe() {
		return scoreEloEquipe;
	}

	public void setScoreEloEquipe(Long scoreEloEquipe) {
		this.scoreEloEquipe = scoreEloEquipe;
	}

	public Long getScroreFairPlayEquipe() {
		return scroreFairPlayEquipe;
	}

	public void setScroreFairPlayEquipe(Long scroreFairPlayEquipe) {
		this.scroreFairPlayEquipe = scroreFairPlayEquipe;
	}

	public String getImageEquipe() {
		return imageEquipe;
	}

	public void setImageEquipe(String imageEquipe) {
		this.imageEquipe = imageEquipe;
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

	@Override
	public String toString() {
		return "EquipeHistorique [idEquipeHistorique=" + idEquipeHistorique + ", dateModification=" + dateModification
				+ ", idStringEquipe=" + idStringEquipe + ", nomEquipe=" + nomEquipe + ", estDispoPourMatch="
				+ estDispoPourMatch + ", estSupprime=" + estSupprime + ", scoreEloEquipe=" + scoreEloEquipe
				+ ", scroreFairPlayEquipe=" + scroreFairPlayEquipe + ", imageEquipe=" + imageEquipe
				+ ", serie_victoire=" + serie_victoire + ", serie_defaite=" + serie_defaite + ", attributModifie="
				+ attributModifie + ", equipe=" + equipe + "]";
	}

	
	
	
	
}
