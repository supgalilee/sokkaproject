package com.sokka.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sokka.model.historique.EquipeHistorique;


@Entity
@Table(name = "equipe")
public class Equipe {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idEquipe;
	private String idStringEquipe;
	private String nomEquipe;
	private Boolean estDispoPourMatch;
	private Boolean estSupprime;
	private String imageEquipe;
	private Long serie_victoire;
	private Long serie_defaite;
	private Long compteur_victoire;
	private Long compteur_defaite;


	@ManyToOne
    @JoinColumn(name="idZone", nullable=false)
	//@JsonManagedReference								
	private Zone zone;
	
	@OneToMany(mappedBy="equipe")
	@JsonBackReference(value="his")
	private Set<EquipeHistorique> equipesHistorique;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JsonBackReference(value="jou")
	@JoinTable(
    joinColumns = { @JoinColumn(name = "idEquipe") },
    inverseJoinColumns = { @JoinColumn(name = "idJoueur") })
	private Set<Joueur> joueurs = new HashSet<>();
	
	@OneToMany(mappedBy = "idEquipe", cascade = CascadeType.ALL)
	@JsonBackReference(value="equipe_categ")
    private Set<EquipeCategorie> equipeCategorie;
	
	@OneToMany(mappedBy = "equipeDomicile", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Match> matchEquipeDomicile;
	
	@OneToMany(mappedBy = "equipeExterieur", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Match> matchEquipeExterieur;

	public void addJoueur(Joueur joueur) {
		joueurs.add(joueur);
	}
	
	public Equipe() {
		super();
	}

	public Equipe(Long idEquipe, String idStringEquipe, String nomEquipe, Boolean estDispoPourMatch,
			Boolean estSupprime, Long serie_victoire, Long serie_defaite ,Long compteur_victoire, Long compteur_defaite , Zone zone, Set<Joueur> joueurs, Set<EquipeCategorie> equipeCategorie,
			Set<Match> matchEquipeDomicile, Set<Match> matchEquipeExterieur) {
		super();
		this.idEquipe = idEquipe;
		this.idStringEquipe = idStringEquipe;
		this.nomEquipe = nomEquipe;
		this.estDispoPourMatch = estDispoPourMatch;
		this.estSupprime = estSupprime;
		this.serie_victoire = serie_victoire;
		this.serie_defaite = serie_defaite;
		this.compteur_victoire = compteur_victoire;	
		this.compteur_defaite = compteur_defaite;
		this.zone = zone;
		this.joueurs = joueurs;
		this.equipeCategorie = equipeCategorie;
		this.matchEquipeDomicile = matchEquipeDomicile;
		this.matchEquipeExterieur = matchEquipeExterieur;
	}



	public Long getIdEquipe() {
		return idEquipe;
	}
	public void setIdEquipe(Long idEquipe) {
		this.idEquipe = idEquipe;
	}
	public String getIdStringEquipe() {
		return idStringEquipe;
	}
	public void setIdStringEquipe(String idStringEquipe) {
		this.idStringEquipe = idStringEquipe;
	}
	public String getNomEquipe() {
		return nomEquipe;
	}
	public void setNomEquipe(String nomEquipe) {
		this.nomEquipe = nomEquipe;
	}
	public Boolean getEstDispoPourMatch() {
		return estDispoPourMatch;
	}
	public void setEstDispoPourMatch(Boolean estDispoPourMatch) {
		this.estDispoPourMatch = estDispoPourMatch;
	}
	

	public Long getSerie_victoire() {
		return serie_victoire;
	}

	public void setSerie_victoire(Long serie_victoire) {
		this.serie_victoire = serie_victoire;
	}

	public Long getSerie_defaite() {
		return serie_defaite;
	}

	public void setSerie_defaite(Long serie_defaite) {
		this.serie_defaite = serie_defaite;
	}
	

	public Long getCompteur_victoire() {
		return compteur_victoire;
	}

	public void setCompteur_victoire(Long compteur_victoire) {
		this.compteur_victoire = compteur_victoire;
	}

	public Long getCompteur_defaite() {
		return compteur_defaite;
	}

	public void setCompteur_defaite(Long compteur_defaite) {
		this.compteur_defaite = compteur_defaite;
	}

	public String getImageEquipe() {
		return imageEquipe;
	}

	public void setImageEquipe(String imageEquipe) {
		this.imageEquipe = imageEquipe;
	}

	public Set<Joueur> getJoueurs() {
		return joueurs;
	}


	public void setJoueurs(Set<Joueur> joueurs) {
		this.joueurs = joueurs;
	}


	public Set<EquipeCategorie> getEquipeCategorie() {
		return equipeCategorie;
	}


	public void setEquipeCategorie(Set<EquipeCategorie> equipeCategorie) {
		this.equipeCategorie = equipeCategorie;
	}


	public Boolean getEstSupprime() {
		return estSupprime;
	}


	public void setEstSupprime(Boolean estSupprime) {
		this.estSupprime = estSupprime;
	}


	public Zone getZone() {
		return zone;
	}


	public void setZone(Zone zone) {
		this.zone = zone;
	}


	public Set<Match> getMatchEquipeDomicile() {
		return matchEquipeDomicile;
	}


	public void setMatchEquipeDomicile(Set<Match> matchEquipeDomicile) {
		this.matchEquipeDomicile = matchEquipeDomicile;
	}


	public Set<Match> getMatchEquipeExterieur() {
		return matchEquipeExterieur;
	}


	public void setMatchEquipeExterieur(Set<Match> matchEquipeExterieur) {
		this.matchEquipeExterieur = matchEquipeExterieur;
	}


	@Override
	public String toString() {
		return "Equipe [idEquipe=" + idEquipe + ", idStringEquipe=" + idStringEquipe + ", nomEquipe=" + nomEquipe
				+ ", estDispoPourMatch=" + estDispoPourMatch + ", estSupprime=" + estSupprime + ", zone=" + zone
				+ ", joueurs=" + joueurs + ", equipeCategorie=" + equipeCategorie + ", matchEquipeDomicile="
				+ matchEquipeDomicile + ", matchEquipeExterieur=" + matchEquipeExterieur + "]";
	}
	
	

}
